package com.sumax.hororate;

public class RateObject {

    private int one;
    private int two;
    private int three;
    private int four;
    private int five;
    private int rate_new;

    private String date;

    public RateObject(){

    }

    /* Damit kann alles relevante für die Statistik abgerufen werden.
     * Konnte keine Arrays benutzen wegen Firebase. Das braucht einfache getter Methoden.
     */
    public RateObject(int[] rate_old, int rate_new, String date){
        this();
        one = rate_old[0];
        two = rate_old[1];
        three = rate_old[2];
        four = rate_old[3];
        five = rate_old[4];
        this.rate_new = rate_new;
        this.date = date;
    }

    public int getOne(){
        return one;
    }

    public int getTwo(){
        return two;
    }

    public int getThree(){
        return three;
    }

    public int getFour(){
        return four;
    }

    public int getFive(){
        return five;
    }

    public void setOne(int i){
        one = i;
    }

    public void setTwo(int i){
        two = i;
    }

    public void setThree(int i){
        three = i;
    }

    public void setFour(int i){
        four = i;
    }

    public void setFive(int i) {
        five = i;
    }

    public int getRate_new() {
        return rate_new;
    }

    public void setRate_new(int rate_new) {
        this.rate_new = rate_new;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
