package com.sumax.hororate;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.os.AsyncTask;
import android.widget.TextView;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;
import java.time.format.DateTimeFormatter;
import java.time.LocalDate;

import static android.content.Context.MODE_PRIVATE;


class RetrieveHoroscopeTask extends AsyncTask<Void, Void, String> {

    private Exception exception;
    private TextView field;
    private Activity activity;
    private String starSign;
    private String apiBase = "http://sandipbgt.com/theastrologer/api/horoscope/";

    public RetrieveHoroscopeTask(Activity activity, TextView field, String starSign){
        this.activity = activity;
        this.field = field;
        this.starSign = starSign.toLowerCase();
    }

    protected String doInBackground(Void... urls) {
        try {
            URL url = new URL(this.apiBase + this.starSign + "/today/");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();

                final JSONObject json = new JSONObject(stringBuilder.toString());
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            field.setText(json.getString("horoscope").replace("(c) Kelli Fox, The Astrologer, http://new.theastrologer.com", ""));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                return stringBuilder.toString();
            }
            finally{
                urlConnection.disconnect();
            }
        }
        catch(Exception e) {
            Log.e("ERROR", e.getMessage(), e);
            return null;
        }
    }

    protected void onPostExecute(String response) {
        if(response == null) {
            response = "THERE WAS AN ERROR";
        }
    }
}