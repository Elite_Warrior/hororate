package com.sumax.hororate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;



public class RegisterActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private static final String TAG = "Register";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        super.onStart();

        // Check if user is signed in (non-null) and update UI accordingly.
        mAuth = FirebaseAuth.getInstance();
        final Button regSubmit = (Button) findViewById(R.id.regSubmitButton);

        //Clicklistener fürs Regestrieren, Passwort wird auf richtigkeit geprüft
        regSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText text = (EditText) findViewById(R.id.regEmail);
                final String email = text.getText().toString();
                text = (EditText) findViewById(R.id.regPassword1);
                final String password = text.getText().toString();
                text = (EditText) findViewById(R.id.regPassword2);
                final String passwordRepeat = text.getText().toString();
                text = (EditText) findViewById(R.id.regDate);
                final String date = text.getText().toString();

                if (email.isEmpty() || password.isEmpty()) {
                    Toast.makeText(RegisterActivity.this, "Email/Password shouldn't be empty", Toast.LENGTH_SHORT).show();
                } else if (!password.equals(passwordRepeat)) {
                    Toast.makeText(RegisterActivity.this, "Password does not equal", Toast.LENGTH_SHORT).show();
                } else if (password.length() < 6) {
                    Toast.makeText(RegisterActivity.this, "Password length must be 6 or greater", Toast.LENGTH_SHORT).show();
                } else if (!isDateFormatCorrect(date)) {
                    Toast.makeText(RegisterActivity.this, "Set date format to dd.MM.yyyy", Toast.LENGTH_SHORT).show();
                } else{
                    registerUser(email,password,date);
                }
            }
        });
    }

    private void setAccountPreference(String email, String password, String date){
        SharedPreferences settings = getSharedPreferences("Account", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("E-Mail",email);
        editor.putString("Password",password);
        editor.commit();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        preferences.edit().putString("starPreference", dateIntoStarsign(date)).commit();
    }

    public boolean isDateFormatCorrect(String dateInput){
        try {
            DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            format.setLenient(false);
            format.parse(dateInput);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public String dateIntoStarsign(String date){
        String month = date.substring(3,5);
        Log.d(TAG, "Month: "+month);
        switch(month){
            case "01": return "Capricorn";
            case "02": return "Aquarius";
            case "03": return "Pisces";
            case "04": return "Aries";
            case "05": return "Taurus";
            case "06": return "Gemini";
            case "07": return "Cancer";
            case "08": return "Leo";
            case "09": return "Virgo";
            case "10": return "Libra";
            case "11": return "Scorpio";
            case "12": return "Sagittarius";
            default : return "Leo";

        }
    }

    private void registerUser(final String email, final String password, final String date){
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "succes");
                    MainActivity.setmAuth(mAuth);
                    setAccountPreference(email,password, date);
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                } else {
                    Log.d(TAG, "error: " + task.getException().getMessage());
                    Toast.makeText(RegisterActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
