package com.sumax.hororate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";

    private SharedPreferences.OnSharedPreferenceChangeListener prefListener;
    private SharedPreferences preferences;
    private static FirebaseAuth mAuth;
    private static Database mDatabase;
    private static RatingBar ratingBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Bisherige Einstellungsauswahl ermitteln
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        Date currentTime = Calendar.getInstance().getTime();
        //Lade Standartfragment zum Anzeigen des Homescreens
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentWrapper, new HomeFragment()).commit();
        initNavigation();
        //Bei Firebase authentisieren, wenn Account dargelegt wird automatisch eingelogt, Datenbank wird aufgerufen
        if (mAuth == null) {
            signIn();
        }

        if(currentTime.getHours() > 18)
            ratingBar.setVisibility(View.VISIBLE);
        else
            ratingBar.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Navigation drawer begrüßt mit dem aktuellen Sternzeichen
        //Gibt es keine zuvor getroffene Auswahl, dann wird standartmäßig das Sternzeichen Leo zurückgegeben
        String sternzeichen = preferences.getString("starPreference", "Leo");
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View navHeader = navigationView.getHeaderView(0);
        TextView navTextview = (TextView) navHeader.findViewById(R.id.nav_textView);
        navTextview.setText(sternzeichen);
        if (mAuth.getCurrentUser() == null) {
            Log.d(TAG,"reAuth");
            signIn();
        }
        Date currentTime = Calendar.getInstance().getTime();
        if(currentTime.getHours() > 18)
            ratingBar.setVisibility(View.VISIBLE);
        else
            ratingBar.setVisibility(View.INVISIBLE);
        setListener();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentWrapper, new HomeFragment()).commit();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(true);
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Date currentTime = Calendar.getInstance().getTime();
        if (id == R.id.nav_home) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentWrapper, new HomeFragment()).commit();
            if(currentTime.getHours() > 18)
                ratingBar.setVisibility(View.VISIBLE);
            else
                ratingBar.setVisibility(View.INVISIBLE);
            navigationView.getMenu().getItem(0).setChecked(true);
        } else if (id == R.id.nav_statistic) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentWrapper, new StatisticsFragment()).commit();
            navigationView.getMenu().getItem(1).setChecked(true);
            ratingBar.setVisibility(View.INVISIBLE);
        } else if (id == R.id.nav_settings) {
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
            navigationView.getMenu().getItem(2).setChecked(true);
        } else if (id == R.id.nav_impressum) {
            //setzt ein falsches datum damit man die statistik mit der bewertung updaten kann.
            mDatabase.getRateObject().setDate("B");
            mDatabase.sync(mAuth.getCurrentUser().getUid(), (int) ratingBar.getRating());
        } else if (id == R.id.nav_privacy) {
        } else if (id == R.id.nav_logout) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initNavigation() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    //Datenbank instanzieren, App aktualisieren
    private void createDatabase(FirebaseUser fUser) {
        mDatabase = Database.getInstance();
        if (fUser == null) {
            Log.d("SignIn", "No user found");
        } else {
            mDatabase.syncDataToApp(fUser.getUid());
            setListener();
            Log.d("DataBase", "return : " + mDatabase.getRateObject().getOne());
        }
    }

    //Ratingbar listener
    private void setListener() {
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Log.d("DataBase", "Listener activated: " + ratingBar.getRating() + " User:" + mAuth.getCurrentUser().getUid());
                if (mAuth.getCurrentUser().getUid().length() <= 0) {
                    Log.d(TAG, mAuth.getCurrentUser().getUid() + " has problems");
                }
                mDatabase.sync(mAuth.getCurrentUser().getUid(), (int) ratingBar.getRating());
            }
        });
    }

    public static void updateRatingbar() {
        if ( Database.getInstance().getRateObject() != null && ratingBar != null) {
            ratingBar.setRating(Database.getInstance().getRating());
        }
    }

    public static void setmAuth(FirebaseAuth auth) {
        if(auth != null){
            mAuth = auth;
            Database.getInstance().syncDataToApp(mAuth.getCurrentUser().getUid());
            Log.d(TAG, " " + mAuth.getCurrentUser().getUid());
        }
    }

    public void signIn(){
        mAuth = FirebaseAuth.getInstance();
        SharedPreferences settings = getSharedPreferences("Account", 0);
        //Log.d(TAG,"e: "+settings.getString("E-Mail","")+" , p: "+ settings.getString("Password",""));
        if (!settings.getString("E-Mail", "").isEmpty()) {
            mAuth.signInWithEmailAndPassword(settings.getString("E-Mail", ""), settings.getString("Password", "")).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "succes " + mAuth.getCurrentUser().getUid());
                        createDatabase(mAuth.getCurrentUser());
                    } else {
                        Log.d(TAG, "error: " + task.getException().getMessage());
                    }
                }
            });
        } else {
            mAuth.signInAnonymously()
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.d("SignIn", "signInAnonymously:success");
                                createDatabase(mAuth.getCurrentUser());
                            } else {
                                Log.d("SignIn", "signInAnonymously:failure", task.getException());
                            }
                        }
                    });
        }
    }

}
