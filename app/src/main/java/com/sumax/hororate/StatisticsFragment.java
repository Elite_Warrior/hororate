package com.sumax.hororate;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.Random;

public class StatisticsFragment extends Fragment {

    private TextView oneCount;
    private TextView twoCount;
    private TextView threeCount;
    private TextView fourCount;
    private TextView fiveCount;
    private TextView durchschnittCount;
    private TextView evaluateCount;
    private RateObject rateObject;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_statistics, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        rateObject = Database.getInstance().getRateObject();
        oneCount = view.findViewById(R.id.oneCount);
        oneCount.setText(Integer.toString(rateObject.getOne()));

        twoCount = view.findViewById(R.id.twoCount);
        twoCount.setText(Integer.toString(rateObject.getTwo()));

        threeCount = view.findViewById(R.id.threeCount);
        threeCount.setText(Integer.toString(rateObject.getThree()));

        fourCount = view.findViewById(R.id.fourCount);
        fourCount.setText(Integer.toString(rateObject.getFour()));

        fiveCount = view.findViewById(R.id.fiveCount);
        fiveCount.setText(Integer.toString(rateObject.getFive()));

        evaluateCount = view.findViewById(R.id.evaluateCount);
        int gesamtBewertet = rateObject.getOne() + rateObject.getTwo() + rateObject.getThree() + rateObject.getFour() + rateObject.getFive();
        Log.i("Gesamt Bewertet:", String.valueOf(gesamtBewertet));
        evaluateCount.setText(Integer.toString(gesamtBewertet));

        durchschnittCount = view.findViewById(R.id.durchschnittCount);

        if(gesamtBewertet != 0){
            double durchschnitt = ((rateObject.getOne()) + (rateObject.getTwo() * 2) + (rateObject.getThree() * 3) + (rateObject.getFour() * 4) + (rateObject.getFive() * 5)) / (double) gesamtBewertet;
            durchschnittCount.setText(new DecimalFormat("#0.00").format(durchschnitt));
        }
        super.onViewCreated(view, savedInstanceState);
    }

}
