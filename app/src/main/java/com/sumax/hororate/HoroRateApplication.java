package com.sumax.hororate;

import android.app.Application;
import com.google.firebase.database.FirebaseDatabase;

public class HoroRateApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
