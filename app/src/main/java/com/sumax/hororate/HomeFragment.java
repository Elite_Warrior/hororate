package com.sumax.hororate;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class HomeFragment extends Fragment {

    TextView horoText;
    private SharedPreferences.OnSharedPreferenceChangeListener prefListener;
    private SharedPreferences preferences;
    private TextView mainHint;
    private TextView textRating;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        //listener on changed sort order preference:
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        prefListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {

                Log.i("Settings key changed: " , key);
                if(key.equals("starPreference"))
                    refreshHorsocope();
            }
        };
        prefs.registerOnSharedPreferenceChangeListener(prefListener);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        //Datum ermitteln und anzeigen
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate localDate = LocalDate.now();
        TextView dateText = view.findViewById(R.id.main_time);
        dateText.setText(dtf.format(localDate));

        horoText = view.findViewById(R.id.main_horoscope);
        refreshHorsocope();
        super.onViewCreated(view, savedInstanceState);
        Date currentTime = Calendar.getInstance().getTime();
        Log.i("Date: ",  currentTime.getHours() + "");
        mainHint = (TextView) view.findViewById(R.id.main_rate_hint);
        textRating = (TextView) view.findViewById(R.id.main_rate);

        if(currentTime.getHours() > 18) {
            mainHint.setVisibility(View.INVISIBLE);
            textRating.setVisibility(View.VISIBLE);
        } else {
            mainHint.setVisibility(View.VISIBLE);
            textRating.setVisibility(View.INVISIBLE);
        }



    }

    public void onResume() {
        Date currentTime = Calendar.getInstance().getTime();
        if(currentTime.getHours() > 18) {
            mainHint.setVisibility(View.INVISIBLE);
            textRating.setVisibility(View.VISIBLE);
        } else {
            mainHint.setVisibility(View.VISIBLE);
            textRating.setVisibility(View.INVISIBLE);
        }
        super.onResume();

    }

    public void refreshHorsocope()
    {
        String sternzeichen = preferences.getString("starPreference", "Leo");
        new RetrieveHoroscopeTask(getActivity(), horoText, sternzeichen).execute();
    }
}
