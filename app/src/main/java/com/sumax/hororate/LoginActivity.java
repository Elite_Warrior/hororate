package com.sumax.hororate;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.ProviderQueryResult;


public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "Login";

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText logEmail = (EditText) findViewById(R.id.logEmail);
        final EditText logPassword = (EditText) findViewById(R.id.logPassword);
        final Button logSignIn = (Button) findViewById(R.id.logButton);
        final TextView logRegister = (TextView) findViewById(R.id.logRegister);
        mAuth = FirebaseAuth.getInstance();
        //final ImageView logHoroRateLogo = (ImageView) findViewById(R.id.logHoroRateLogo);
        //Beim Klick auf Register Here wechseln wir zur RegisterActivity
        logRegister.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                // Das zweite Argument wird dann durch den Klick aufgerufen mit startActivity-Methode
                Intent regIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                regIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                LoginActivity.this.startActivity(regIntent);
            }
        });
        //Gesicherte Daten werden geladen
        SharedPreferences settings = getSharedPreferences("Account", 0);
        logEmail.setText(settings.getString("E-Mail", "").toString());
        logPassword.setText(settings.getString("Password", "").toString());

        //Login Listener für Email und Passwort
        logSignIn.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v){
                EditText text = (EditText) findViewById(R.id.logEmail);
                final String email = text.getText().toString();
                text = (EditText) findViewById(R.id.logPassword);
                final String password = text.getText().toString();
                if(!email.isEmpty() && !email.equals(getSharedPreferences("Account", 0).getString("E-Mail",""))){
                    login(email,password);
                } else if (email.isEmpty()){
                    Toast.makeText(LoginActivity.this, "Email wrong", Toast.LENGTH_SHORT).show();
                } else {
                    logout();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        SharedPreferences settings = getSharedPreferences("Account", 0);
        if(!settings.getString("E-Mail","").isEmpty()){
            Button button = (Button) findViewById(R.id.logButton);
            button.setText("Logout");
            button = (Button) findViewById(R.id.logRegister);
            button.setVisibility(View.GONE);
        } else {
            Button button = (Button) findViewById(R.id.logButton);
            button.setText("Login");
            button = (Button) findViewById(R.id.logRegister);
            button.setVisibility(View.VISIBLE);
        }
        super.onResume();
    }

    private void setAccountPreference(String email, String password){
        SharedPreferences settings = getSharedPreferences("Account", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("E-Mail",email);
        editor.putString("Password",password);
        editor.commit();
    }

    private void login(final String email, final String password){
            mAuth.fetchProvidersForEmail(email).addOnCompleteListener(new OnCompleteListener<ProviderQueryResult>() {
                @Override
                public void onComplete(@NonNull Task<ProviderQueryResult> task) {
                    if(task.isSuccessful()){
                        Log.d(TAG, "Is listed "+task.getResult().getProviders().size());
                        if(task.getResult().getProviders().size() > 0 && !password.isEmpty()){
                            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        Log.d(TAG, "succes "+mAuth.getCurrentUser().getUid());
                                        MainActivity.setmAuth(mAuth);
                                        //Account wird in der App hinterlegt zum Autologin
                                        setAccountPreference(email,password);
                                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                    } else {
                                        Log.d(TAG, "error: " + task.getException().getMessage());
                                        Toast.makeText(LoginActivity.this, "Password is incorrect", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        } else if(password.isEmpty()){
                            Toast.makeText(LoginActivity.this, "Password needed", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(LoginActivity.this, "Email does not exist", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
    }

    private void logout(){
        //Bei einem Logout werden die Preferenzen auf leer gesetzt
        setAccountPreference("","");
        mAuth.signOut();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }
}
