package com.sumax.hororate;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Database {

    private final String TAG = "Database";
    private static Database mInstance;
    private FirebaseDatabase fBase;
    private DatabaseReference fBaseRef;
    private RateObject mRateObject;

    public static synchronized Database getInstance(){
        if (mInstance == null) {
            mInstance = new Database();
        }
        return mInstance;
    }

    public Database(){
        fBase = FirebaseDatabase.getInstance();
        fBaseRef = fBase.getReference();
        mRateObject = new RateObject();
        mRateObject.setDate("");
        Log.d(TAG, "Database created");
    }

    public int getRating(){
        return mRateObject.getRate_new();
    }

    public RateObject getRateObject(){
        return mRateObject;
    }

    //Daten werden von Firebase in die App geladen, wenn keine vorhanden sind wird alles auf 0 gesetzt.
    public void syncDataToApp(final String userID){
        fBaseRef.child(userID).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        mRateObject = dataSnapshot.getValue(RateObject.class);
                        if(mRateObject==null){
                            mRateObject = new RateObject();
                            mRateObject.setDate(getDate());
                            Log.d(TAG, "Rateobject created, date: " + mRateObject.getDate());
                        }
                        MainActivity.updateRatingbar();
                        Log.d(TAG, "Sync with id: "+ userID + " successful." + " rate: " + mRateObject.getRate_new());
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d(TAG, "Data Sync failed ");
                    }
                });
    }

    //überprüft das Datum des letzten aufrufes. Addiert Bewertung in das. Sollte täglich passieren
    public void sync(final String userID, int actualRating){
        if( !mRateObject.getDate().equals(getDate())){
            switch(mRateObject.getRate_new()){
                case 1 : mRateObject.setOne(mRateObject.getOne()+1); break;
                case 2 : mRateObject.setTwo(mRateObject.getTwo()+1); break;
                case 3 : mRateObject.setThree(mRateObject.getThree()+1); break;
                case 4 : mRateObject.setFour(mRateObject.getFour()+1); break;
                case 5 : mRateObject.setFive(mRateObject.getFive()+1); break;
            }
            mRateObject.setDate(getDate());
            mRateObject.setRate_new(actualRating);
            syncDataToFirebase(userID);
            Log.d(TAG, "Is synced, added to statistics");
        }else{
            mRateObject.setRate_new(actualRating);
            syncDataToFirebase(userID);
            Log.d(TAG, "Is synced");
        }
    }

    //Daten werden in Firebase hochgeladen
    public void syncDataToFirebase(String userID){
        fBaseRef.child(userID).setValue(mRateObject);
    }

    //Transformation des System Datums in String
    public String getDate(){
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.YYYY");
        return dateFormat.format(date);
    }
}